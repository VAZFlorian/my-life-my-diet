var express = require("express");
var router = express.Router();

let Utilisateur = require("../private/models/Utilisateur");
let Historiques = require("../private/models/Historiques");

let secureInputs = (data) => {
  Object.keys(data).forEach(
    (key) =>
      (data[key] = data[key].replace(
        /<.*?>|select|from|where|update|insert/g,
        ""
      ))
  );
};

var session;


router.get("/", function (req, res) {
    if (req.session.user) {
        res.send("Vous etes deja connecté <a href=\'/logout'>Cliquez ici pour vos deconnecter</a>");
      } else
        res.render('acceuil', { session: req.session });
});


router.post("/login", function (req, res) {
    secureInputs(req.body);
    console.log(req.body);
    session = req.session;
    Utilisateur.connection(req.body.login, req.body.password, function (rows) {


        session.user = rows[0]
        console.log(session);
        // Si on a trouver un utilisateur
        if (rows.length > 0) {
            res.redirect('profil')
        }
        else {
          res.send("Vos identifiants sont incorrects, <a href=\'/'>cliquer pour revenir à l'acceuil</a>");
        }
    })
    
});

router.get("/register", function (req, res) {
    res.render("register");
});

router.post("/register", function (req, res) {
    secureInputs(req.body);
    console.log(req.body);

    Utilisateur.verifCompte(req.body.login, (compteexistant)=>{

    console.log(compteexistant)
    if (compteexistant) {
        res.send("Ce login est dejà utilisé, choisissez en un autre <a href=\'/register'>cliquer pour revenir en arrière</a>");
    }
    else {
        Utilisateur.register(req.body.login, req.body.password, req.body.nom, req.body.prenom, req.body.datenaissance, req.body.email);
        res.send("Compte créer, <a href=\'/'>cliquer pour revenir à l'acceuil</a>");
    }
})
});

router.get("/profil", function (req, res) {
    console.log(req.session.user)
    if ( typeof(req.session.user) != "undefined" ) {

        Historiques.getHistorique(req.session.user.id, (result)=>{
            console.log(result)
            res.render("profil",  { session: req.session, historiques : result });
        })
        
    }
    else {
        res.redirect('/')
    }
});

router.post("/updateaccount", function (req, res) {
    secureInputs(req.body)
    console.log(req.session)
    Utilisateur.updateinfos(req.session.user.id, req.body.nom, req.body.prenom, req.body.datenaissance, req.body.email,
      function () {
    
        Utilisateur.connection(req.session.user.login, req.session.user.password,
            function (rows) {
                console.log(rows)
                req.session.user = rows[0]
                res.redirect('profil')
        })
    })
});

router.post("/deleteaccount", function (req, res) {
    secureInputs(req.body);
    // console.log(req.body);
    // Utilisateur.register(req.body.username, req.body.password);
    res.render("profil");
});

router.post("/updateresults", function (req, res) {
    secureInputs(req.body);
    console.log(req.body);
    // 
    let niveauactivite
    switch (req.body.niveauActivite) {
        case "sedentaire" :
            niveauactivite = 0
            break;
        case "actiffaible" :
            niveauactivite = 1
            break;
        case "actif":
            niveauactivite = 2
            break;
        default:
            niveauactivite = 3
    }

    Historiques.addHistorique(req.session.user.id, req.body.IMC, req.body.BJ, ()=>{
        Utilisateur.updateDefaut(req.session.user.id, req.body.gender, req.body.taille, req.body.poids, niveauactivite, ()=>{
            Utilisateur.connection(req.session.user.login, req.session.user.password,
                function (rows) {
                    console.log(rows)
                    req.session.user = rows[0]
                    res.redirect('profil')
            })
        })
    })
});

router.get("/logout", function(req, res) {
    req.session.destroy();
    res.redirect('/')
})

router.get("/deleteAccount", function(req, res) {

    Historiques.deleteHistorique(req.session.user.id, ()=>{
        Utilisateur.deleteAccount(req.session.user.id, ()=> {
            req.session.destroy();
            res.send("Votre compte à bien été supprimé, <a href=\'/'>cliquer pour revenir à l'acceuil</a>");
        })
    })
})

module.exports = router;


