// nav 

document.getElementById('menu-icon').addEventListener('click', function() {
    document.querySelectorAll('.navbar').forEach( e => {
      e.classList.toggle('expand')
    })
  })


document.getElementById('datenaissance').max = new Date().toISOString().split("T")[0];




let taille = document.getElementById('taille')
let poids = document.getElementById('poids')
let age = document.getElementById('age')

let gender = document.getElementById('gender')
let genderinput = document.getElementById('genderinput')

let titreIMC = document.getElementById('titreIMC')
let displayIMC = document.getElementById('displayIMC')
let descriptionIMC = document.getElementById('descriptionIMC')

let displayBJ = document.getElementById('displayBJ')


let dataIMC = { 
    maigre : ["maigre", "-95deg"], 
    morbide : ["morbide", "100deg"],
    sévère : ["obesité sévère", "65deg"],
    modérée : ["modérée", "10deg"],
    surpoid : ["surpoid", "-15deg"],
    normale : ["normale", "-45deg"]
}








getNiveauActivite = () => {
    return document.querySelector('input[name="niveauActivite"]:checked').value;
}


toggleGender = e => {
    
    console.log(e.target.getAttribute("data-isMan"))
    if (e.target.getAttribute("data-isMan") == "1"){

        e.target.setAttribute("src", "/assets/images/femme.png")
        e.target.setAttribute("data-isMan", "0")
        genderinput.setAttribute("value", 0) 
    }
    else {
        e.target.setAttribute("src", "/assets/images/homme.png")
        e.target.setAttribute("data-isMan", "1")
        genderinput.setAttribute("value", 1) 
    }
    updateBJ()
}

gender.addEventListener("click", toggleGender)





getCouleur = (value) => {
    switch (true) {
        case value < 18.4 :
            return "maigre";
            break;
        case value > 39.9 :
            return "morbide";
            break;
        case value > 34.9 :
            return "sévère";
            break;
        case value > 29.9 :
            return "modérée";
            break;
        case value > 24.9 :
            return "surpoid";
            break;
        default:
            return "normale";
    }
}



let updateIMC = ()=>{

    result = Math.round( 100 * poids.value / ((taille.value*0.01)**2)) / 100
    IMClevel = getCouleur(result)
    console.log(IMClevel)
    titreIMC.classList = IMClevel

    displayIMC.innerHTML = result
    displayIMC.classList = IMClevel

    descriptionIMC.innerHTML = `Vous êtes considéré comme étant ${dataIMC[IMClevel][0]}`
    descriptionIMC.classList = IMClevel

    document.getElementById('resultatIMC').setAttribute("value", result) 

    console.log(`rotate(${dataIMC[IMClevel][1]});`)
    document.getElementById('aiguille').style.transform = `rotate(${dataIMC[IMClevel][1]})`
    
}

let updateBJ = ()=>{
    if (gender.getAttribute("data-isMan") == "1") {
        resultattempo = (13.707*poids.value) + (492.3 * taille.value * 0.01) - (6.673 * age.value) + 77.607
        
    }
    else {
        resultattempo = (9.740*poids.value) + (172.9 * taille.value * 0.01) - (4.737 * age.value) + 667.051
    }

    switch (true) {
        case getNiveauActivite() == "sedentaire" :
            resultat = resultattempo * 1.2
            break;
        case getNiveauActivite() == "actiffaible" :
            resultat = resultattempo * 1.375
            break;
        case getNiveauActivite() == "actif" :
            resultat = resultattempo * 1.55
            break;
        default:
            resultat = resultattempo * 1.725
    }
    resultat = Math.round(resultat * 100) / 100
    displayBJ.innerHTML = resultat
    document.getElementById('resultatBJ').setAttribute("value", resultat) 
}


let updateBoth = ()=> {
    updateIMC()
    updateBJ()
}




poids.addEventListener('change', updateBoth)
taille.addEventListener('change', updateBoth)


age.addEventListener('change', updateBJ) 


document.querySelectorAll('input[name="niveauActivite"]').forEach( e => {
    e.addEventListener('change', updateBJ)
})



// Affichage de la progression

for (let i = 0; i < dataHistoriques.length - 1; i++) {
    
    if (dataHistoriques[i+1].imc - dataHistoriques[i].imc >= 0) {
        document.getElementById('listeprogressIMC').children[i].children[0].setAttribute("src", "/assets/images/thumbsup.png")
        //dataHistoriques[i].progress = "thumbsup"
    }
    else {
        //dataHistoriques[i].progress = "thumbsdown"
        document.getElementById('listeprogressIMC').children[i].children[0].setAttribute("src", "/assets/images/thumbsdown.png")
    }
    
}

Array.from(document.getElementById('listevaluesIMC').children).forEach( e => {
    e.className = getCouleur(parseFloat(e.innerHTML))
})


console.log(dataHistoriques)



let returnage = function (valueannée, valuemois, valuejour) {
    let d = new Date();
    let age = 0;
    let jour_actuel = d.getUTCDate();
    let mois_actuel = d.getUTCMonth();
    let année_actuel = d.getUTCFullYear();


    if (valuemois < mois_actuel) {
        age = année_actuel - valueannée;
    } else if (valuemois - 1 == mois_actuel && valuejour < jour_actuel) {
        age = année_actuel - valueannée;
    } else {
        age = année_actuel - valueannée - 1;
    }
    return age;
}


 

