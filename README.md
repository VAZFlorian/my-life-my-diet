# My life, my diet



## Installation du projet

- Ouvrez un terminal
- Cloner le projet : ```git clone https://gitlab.com/VAZFlorian/my-life-my-diet.git```
- Installer les dependances depuis le fichier package.json : ```npm install```


## Création de la base de données

- Dans le dossier private, editez la ligne 4 du script init.sql en remplacant 'bob' par votre utilisateur mysql (ainsi que le serveur si necessaire).

- Sauvegarder et executez le script init.sql


## Configuration de la connection mysql

- Editez le fichier connectionmysql.js et remplacez les valeurs de user et password par celles correspondant à votre utilisateur mysql

- Sauvegardez le fichier.


## Lancement du projet 

-  ```npm run start``` ou ```node server.js```



