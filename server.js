// Dependances
const express = require('express')
let bodyParser = require('body-parser')
const mysql = require('mysql2');
const cookieParser = require("cookie-parser"); // inutile ?
let sessions = require('express-session')
let app = express()


// Middlewares
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

app.use(cookieParser());
app.use(sessions({
    secret: 'mdpsecret',
    resave: false,
    saveUninitialized: true,
    // cookie: { secure: false } // true pour https
    cookie: { maxAge: 1000 * 60 * 60 * 24} // 24 heures
}))


app.use('/assets', express.static('public'))

app.set('view engine', 'ejs')


// Routeur
var mainRouter = require('./routes/main');
app.use('/', mainRouter);


// 404
app.get('*', function(req, res){
    res.send(`La page que vous essayer d'atteindre n'existe pas, <a href=\'/'>cliquer ici </a> pour revenir à l'index`);
});

app.listen(8000)
