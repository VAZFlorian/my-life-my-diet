DROP DATABASE IF EXISTS myLifemyDiet ;
CREATE DATABASE myLifemyDiet;
USE myLifemyDiet;
GRANT ALL PRIVILEGES ON myLifemyDiet.* TO 'bob'@'localhost';
-- Remplacer Bob par le nom de votre utilisateur mysql

CREATE TABLE Utilisateur (
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    login varchar(255),
    password varchar(100),
    nom varchar(255),
    prenom varchar(255),
    datenaissance varchar(100),
    email varchar(100),
    sexe tinyint(1) , 
    dernieretaille int,
    dernierpoid int,
    niveauactivite int
);


CREATE TABLE Historiques (
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    utilisateur int,
    imc float,
    besoinjournalier float,
    date TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    FOREIGN KEY (utilisateur) REFERENCES Utilisateur(id)
);