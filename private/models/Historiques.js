let connection = require('../connectionmysql')

class Historiques {

    
    
    static addHistorique(utilisateur, imc, besoinjournalier, callback) {
        connection.execute(
          'INSERT INTO Historiques SET utilisateur = ?, imc = ?, besoinjournalier = ?',
          [utilisateur, imc, besoinjournalier],
          (err, result) => {
          
              if (err) throw err
              callback()
          })
      }
      
      static getHistorique(id, callback) {
        connection.query(
            'SELECT * FROM Historiques WHERE utilisateur = ? ORDER BY date DESC LIMIT 10', [id],
            

            (err, rows) => {
            
                if (err) throw err
                //console.log(rows[0].login)
                //return rows
                callback(rows)
            })

        
            
    }
    
    static deleteHistorique(id, callback) {
        connection.execute(
          'DELETE FROM Historiques WHERE  utilisateur = ?',
          [id],
          (err, result) => {
              if (err) throw err
              callback();
          })
        
      }



}

module.exports = Historiques