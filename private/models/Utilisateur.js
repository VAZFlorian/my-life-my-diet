let connection = require('../connectionmysql')

class Utilisateur {


    static register(login, password, nom, prenom, datenaissance, email) {
        connection.execute(
          'INSERT INTO Utilisateur SET login = ?, password = ?, nom = ?, prenom = ?, datenaissance = ?, email = ?, sexe = 0, dernieretaille = 1, dernierpoid = 1, niveauactivite = 0',
          [login, password, nom, prenom, datenaissance, email],
          (err, result) => {
          
            if (err) throw err
   
          })
        
      }
      
      // select * from Utilisateur where login = 'client' AND password = 'client' limit 1;
      static connection(login, password, callback) {
            connection.query(
                'SELECT * FROM Utilisateur WHERE login = ? AND password = ? LIMIT 1', [login, password],
                

                (err, rows) => {
                
                    if (err) throw err
                    //console.log(rows[0].login)
                    //return rows
                    callback(rows)
                })
        }


    

        static updateinfos(id, nom, prenom, datenaissance, email, callback) {
            connection.execute(
              'UPDATE Utilisateur SET nom = ?, prenom = ?, datenaissance = ?, email = ? WHERE id = ? ',
              [nom, prenom, datenaissance, email, id],
              (err, result) => {
                  if (err) throw err
                  callback();
              })
            
          }


          static deleteAccount(id, callback) {
            connection.execute(
              'DELETE FROM Utilisateur WHERE id = ?',
              [id],
              (err, result) => {
                  if (err) throw err
                  callback();
              })
            
          }

          static updateDefaut(id, sexe, taille, poid, niveauactivite, callback) {

            connection.execute(
              'UPDATE Utilisateur SET sexe = ?, dernieretaille = ?, dernierpoid = ?, niveauactivite = ? WHERE id = ? ',
              [sexe, taille, poid, niveauactivite, id],
              (err, result) => {
                  if (err) throw err
                  callback();
              })
            
          }

          static verifCompte(login, callback) {
            connection.query(
                'SELECT * FROM Utilisateur WHERE login = ? LIMIT 1', [login],
                

                (err, rows) => {
                
                    if (err) throw err
                    console.log(rows.length)
                    if (rows.length > 0) {
                        callback("probleme")
                    }
                    else {
                        callback()
                    }
                })
        }

}

module.exports = Utilisateur